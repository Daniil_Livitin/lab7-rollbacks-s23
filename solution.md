# Solutions

## Part 1

To ensure atomic updates of both balance and stock, a possible approach would be to utilize a transaction. The revised code incorporates this solution by initiating the transaction with a BEGIN statement, and concluding it with a COMMIT statement. In case any queries fail, the ROLLBACK statement is executed to revert all modifications made during the transaction.


## Part 2
I created a table Inventory with constraint that amount should not be greater than 100.
```sql
CREATE TABLE Inventory (
username TEXT UNIQUE,
product TEXT UNIQUE,
amount INT CHECK(amount >= 0),
CONSTRAINT inventory_pk PRIMARY KEY (username, product),
CONSTRAINT limit CHECK (amount <= 100));
```

In case the total of the current inventory and the newly purchased quantity surpasses 100, an exception will be raised, and the transaction will be rolled back.